package com.android4dev.navigationview.app;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Android1 on 8/10/2015.
 */
public class AppController //extends Application
 {
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
     //private Context context;

    public static final String CLASS_NAME = AppController.class.getSimpleName();

     private AppController(){
     }
   /* @Override
    public void onCreate(){
        super.onCreate();
        mInstance = this;
    }
    */

    public static synchronized AppController getInstance(){
        if(mInstance == null){
            mInstance = new AppController();
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            //getApplicationContext()
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag, Context context){
        request.setTag(TextUtils.isEmpty(tag) ? CLASS_NAME : tag);
        getRequestQueue(context).add(request);
    }

    public void cancelPendingRequest(Object tag){
        if(mRequestQueue != null){
            mRequestQueue.cancelAll(tag);
        }
    }
}
