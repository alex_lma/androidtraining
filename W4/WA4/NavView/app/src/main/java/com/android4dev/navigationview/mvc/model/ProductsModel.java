package com.android4dev.navigationview.mvc.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android4dev.navigationview.MainActivity;
import com.android4dev.navigationview.app.AppController;
import com.android4dev.navigationview.model.Product;
import com.android4dev.navigationview.model.adpaters.ProductAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Android1 on 8/24/2015.
 */
public class ProductsModel {

    private static final String URL_JSON_ARRAY_PRODUCT = "https://dl.dropboxusercontent.com/u/1559445/ASOS/SampleApi/anyproduct_details.json?catid=1760169";

    private static final String TAG_ASSOCIATED_PRODUCTS = "AssociatedProducts";
    private static final String TAG_BASE_PRICE = "BasePrice";
    private static final String TAG_PRODUCT_IMG_URL = "ProductImageUrls";

    private ArrayList<Product> productList;

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public JsonObjectRequest getJsonObjReq(final RecyclerView.Adapter adapter) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                URL_JSON_ARRAY_PRODUCT,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(MainActivity.class.getSimpleName(), "RESPUESTA: " + response.toString());
                        //Toast.makeText(getActivity(), "RESPUESTA: " + response.toString(), Toast.LENGTH_LONG).show();

                        String jsonStr = response.toString();

                        if (jsonStr != null) {

                            try {

                                JSONArray products = response.getJSONArray(TAG_ASSOCIATED_PRODUCTS);

                                for (int i = 0; i < products.length(); i++) {
                                    JSONObject jo = products.getJSONObject(i);
                                    Product product = new Product(jo.getJSONArray(TAG_PRODUCT_IMG_URL).get(0).toString(), jo.getString(TAG_BASE_PRICE));
                                    productList.add(product);
                                    //Log.d(getActivity().class.getSimpleName(), "Product " + product);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.e("ServiceHandler", "Couldn't get any data from the url");
                        }

                        adapter.notifyDataSetChanged();


                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(MainActivity.class.getSimpleName(), "Error: " + error.getMessage());
                        //Toast.makeText(getActivity(), "ERROR!!! ", Toast.LENGTH_LONG).show();
                    }
                });
        return jsonObjReq;
    }
}
