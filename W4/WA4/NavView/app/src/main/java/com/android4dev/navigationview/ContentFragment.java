package com.android4dev.navigationview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android4dev.navigationview.app.AppController;
import com.android4dev.navigationview.model.Product;
import com.android4dev.navigationview.model.adpaters.ProductAdapter;
import com.android4dev.navigationview.mvc.controller.ProductsController;
import com.android4dev.navigationview.mvc.model.ProductsModel;
import com.android4dev.navigationview.mvc.view.ProductsView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Admin on 04-06-2015.
 */
public class ContentFragment extends Fragment {

    public static RecyclerView recyclerView;
    //public static RecyclerView.LayoutManager layoutManager;
    //public static RecyclerView.Adapter adapter;

    private ProductsModel model;
    private ProductsView view;
    private ProductsController controller;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment,container,false);
        recyclerView = (RecyclerView) v.findViewById(R.id.products_recycler_view);

        model = new ProductsModel();
        view = new ProductsView(this);
        controller = new ProductsController(model, view, getActivity());

        controller.control();

        return v;
    }
}
