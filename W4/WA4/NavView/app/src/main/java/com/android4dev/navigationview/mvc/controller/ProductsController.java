package com.android4dev.navigationview.mvc.controller;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android4dev.navigationview.app.AppController;
import com.android4dev.navigationview.model.Product;
import com.android4dev.navigationview.model.adpaters.ProductAdapter;
import com.android4dev.navigationview.mvc.model.ProductsModel;
import com.android4dev.navigationview.mvc.view.ProductsView;

import java.util.ArrayList;

/**
 * Created by Android1 on 8/24/2015.
 */
public class ProductsController {

    private ProductsModel model;
    private ProductsView view;
    private Context context;

    public ProductsController(ProductsModel model, ProductsView view, Context context){
        this.model = model;
        this.view = view;
        this.context = context;
    }

    public void control(){

        ArrayList<Product> productList = new ArrayList<Product>();
        model.setProductList(productList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        RecyclerView.Adapter adapter = new ProductAdapter(context, productList);

        view.setLayoutManager(layoutManager);
        view.setAdapter(adapter);

        JsonObjectRequest jsonObjReq = model.getJsonObjReq(adapter);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, "", context.getApplicationContext());

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

    }
}
