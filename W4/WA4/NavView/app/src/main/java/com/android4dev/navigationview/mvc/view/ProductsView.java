package com.android4dev.navigationview.mvc.view;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android4dev.navigationview.ContentFragment;
import com.android4dev.navigationview.model.Product;
import com.android4dev.navigationview.model.adpaters.ProductAdapter;

import java.util.ArrayList;

/**
 * Created by Android1 on 8/24/2015.
 */
public class ProductsView {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    public ProductsView(ContentFragment contentFragment){
        recyclerView = contentFragment.recyclerView;
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setProductsDataSet(ArrayList<Product> productsList){

    }


    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        recyclerView.setLayoutManager(this.layoutManager);
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        recyclerView.setAdapter(this.adapter);
    }
}
