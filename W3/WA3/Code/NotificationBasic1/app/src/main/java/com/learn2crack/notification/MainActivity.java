package com.learn2crack.notification;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.support.v4.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	Button Start;
	NotificationCompat.Builder notification;
	PendingIntent pIntent;
	NotificationManager manager;
	Intent resultIntent;
	TaskStackBuilder stackBuilder;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		Start = (Button)findViewById(R.id.start);
		Start.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				showNotification();
							    
			}
			
			public void showNotification(){
				 
			        // define sound URI, the sound to be played when there's a notification
			       Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			         
			        // intent triggered, you can add other intent for other actions
			        Intent intent = new Intent(MainActivity.this, Result.class);
			        PendingIntent pIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);
			         
			        // this is it, we'll build the notification!
			        // in the addAction method, if you don't want any icon, just set the first param to 0
			        Notification mNotification = new Notification.Builder(MainActivity.this)
			             
			            .setContentTitle("New Post!")
			            .setContentText("Here's an awesome update for you!")
			            .setSmallIcon(R.drawable.ic_launcher)
			            .setContentIntent(pIntent)
			            .setSound(soundUri)
			             
			           // .addAction(R.drawable.ic_launcher, "View", pIntent)
			           // .addAction(0, "Remind", pIntent)
			             
			            .build();
			         
			        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			 
			        
			     // If you want to hide the notification after it was selected, do the code below
			     //    mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
			         
			        notificationManager.notify(0, mNotification);
			    }
		
		});
	}



}
